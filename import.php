<?php
/**
 * Created by PhpStorm.
 * User: Sergey Pavlov
 * Date: 10.07.2018
 * Usage: php import.php shops.csv
 * shops.csv in current dir or path to any other csv file
 */

require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();
$pathDB = __DIR__ . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'app.db';

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_sqlite',
        'path' => $pathDB,
    ),
));

$sql = 'CREATE TABLE IF NOT EXISTS shops (
    id INTEGER PRIMARY KEY,
    title VARCHAR(256),
    regionId INT,
    city VARCHAR(256),
    address VARCHAR(256),
    userId INT
)';

list($_, $path) = $argv;

$app['db']->query($sql);
$imported = file_get_contents($path);

$app['shopImport'] = $app->factory(function () {
    return new ShopImport();
});

$app['importCSV'] = function () use ($imported) {
    return new ImportCSV($imported);
};

$db = $app['db'];
$data = $app['importCSV']->getData();
$rows = [];
$qw = [];
foreach ($data as $row) {
    /** @var ShopImport $import */
    $import = $app['shopImport'];
    $import->set($row);
    $rows[] = $import->get();
    if ($import->validate()) $qw[] = $import->save($db);
}

echo 'Imported: ' . count($qw) . ' rows';