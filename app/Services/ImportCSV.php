<?php

/**
 * Created by PhpStorm.
 * User: Сергей Павлов
 * Date: 10.07.2018
 * Time: 18:08
 */
class ImportCSV
{

    private $imported;
    private $data;

    private $inCharset = "cp1251";
    private $outCharset = "UTF-8";

    /**
     * ShopImport constructor.
     * @param $imported
     */
    public function __construct($imported)
    {

        $this->imported = $imported;
        $conv = iconv($this->inCharset, $this->outCharset, $imported);
        $this->data = $this->getCSVdata($conv);

    }

    /**
     * Импорт текста csv файла в ассоциативный массив
     * ( заголовок столбца => значение)
     * @param string $text
     * @return string[]
     */
    public function getCSVdata($text)
    {
        $csv = array_map('str_getcsv', explode("\n", $text));
        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        return $csv;
    }

    /**
     * @return string[]
     */
    public function getData()
    {
        return $this->data;
    }


}