<?php

/**
 * Created by PhpStorm.
 * User: Сергей Павлов
 * Date: 10.07.2018
 */
class ShopImport
{

    /*** @var int */
    public $id;
    /*** @var string */
    public $title;
    /*** @var int */
    public $regionId;
    /*** @var string */
    public $city;
    /*** @var string */
    public $address;
    /*** @var int */
    public $userId;

    /*** @var string[] - связь свойств модели и заголовков таблицы CSV */
    private $columnNames = ['title' => 'TITLE', 'regionId' => 'REGION_ID', 'city' => 'CITY', 'address' => 'ADDR', 'userId' => 'USER_ID'];


    /**
     * Установка свойств модели по импортированным данным строки таблицы
     * @param string[] $row - ассоциативный массив данных, импортированный из файла csv
     *                      ( заголовок столбца => значение)
     */
    public function set($row)
    {
        $this->title = $row[$this->columnNames['title']];
        $this->address = $row[$this->columnNames['address']];
        $this->regionId = $row[$this->columnNames['regionId']];
        $this->city = $row[$this->columnNames['city']];
        $this->userId = $row[$this->columnNames['userId']];
    }


    /**
     * Запрос данных модели в виде ассоциативного массива
     * @return array
     */
    public function get()
    {
        return ['title' => $this->title, 'address' => $this->address, 'regionId' => $this->regionId,
            'city' => $this->city, 'userId' => $this->userId];
    }


    /**
     * Проверка данных модели на корректность
     * (параметры-строки непустые, параметры-индексы целые)
     * @return bool
     */
    public function validate()
    {
        return (!empty(trim($this->title)) AND
            !empty(trim($this->address)) AND
            !empty(trim($this->city)) AND
            ((string)(int)$this->regionId == $this->regionId) AND
            ((string)(int)$this->userId == $this->userId)
        );
    }


    /**
     * Сохранение подготовленных данных модели в запись базы данных
     * @param \Doctrine\DBAL\Connection $db
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function save(\Doctrine\DBAL\Connection $db)
    {
        $insert = $this->get();
        $query = $db->createQueryBuilder();
        $query
            ->insert('shops')
            ->setValue('title', '?')
            ->setValue('address', '?')
            ->setValue('regionId', '?')
            ->setValue('city', '?')
            ->setValue('userId', '?')
            ->setParameter(0, $this->title)
            ->setParameter(1, $this->address)
            ->setParameter(2, $this->regionId)
            ->setParameter(3, $this->city)
            ->setParameter(4, $this->userId);
        return $query->execute();

    }


}